
# e3-essioc  
ESS Site-specific EPICS module : essioc

## Description

This module will include the following modules in an IOC:
* autosave
* iocstats
* caputlog
* auth
* recsync

## Usage
The following lines should be added to an IOC startup script to load and
configure this module:

```
require essioc

iocshLoad("$(essioc_DIR)/common_config.iocsh")
```

The following environment variables need to be defined, either in the IOC's
`env.sh` file or via epicsEnvSet commands. These variables will be
automatically set by the IOC deployment system, but need to be manually
assigned for IOCs that are manually deployed.

* IOCNAME
* IOCDIR 
* AS\_TOP

